package org.gaetani.pdv.backend.service;

/**
 * Created by gaetani on 7/31/16.
 */

import org.gaetani.pdv.backend.model.Product;

public class ProductService extends GenericService<Product> implements IProductService {


    public ProductService() {
        super();
    }
}
