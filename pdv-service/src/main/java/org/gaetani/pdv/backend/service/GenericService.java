package org.gaetani.pdv.backend.service;



import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import com.couchbase.lite.*;
import com.couchbase.lite.util.Log;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.gaetani.pdv.backend.exception.NotAcceptableException;
import org.gaetani.pdv.backend.model.Idable;

/**
 * Created by gaetani on 8/1/16.
 */
public abstract class GenericService<T extends Idable> implements ICrudService<T, String>{


    private Manager manager;
    protected Database database;
    private T instance;
    private static final ObjectMapper MAPPER = new ObjectMapper();

    protected GenericService(){

        try {
            instance = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).newInstance();


            manager = new Manager(new JavaContext(), Manager.DEFAULT_OPTIONS);

            manager.setStorageType("ForestDB");

            database = manager.getDatabase(instance.getClass().getSimpleName().toLowerCase());



            Manager.enableLogging("BlobStore", Log.VERBOSE);
            Manager.enableLogging("CBLite", Log.VERBOSE);
            Manager.enableLogging("ChangeTracker", Log.VERBOSE);
            Manager.enableLogging("Database", Log.VERBOSE);
            Manager.enableLogging("Listener", Log.VERBOSE);
            Manager.enableLogging("MultistreamWriter", Log.VERBOSE);
            Manager.enableLogging("Query", Log.VERBOSE);
            Manager.enableLogging("RemoteRequest", Log.VERBOSE);
            Manager.enableLogging("Router", Log.VERBOSE);
            Manager.enableLogging("Sync", Log.VERBOSE);
            Manager.enableLogging("View", Log.VERBOSE);

        } catch (InstantiationException | IllegalAccessException | ClassCastException | IOException |CouchbaseLiteException  ex) {
            throw new NotAcceptableException(ex);
        }
    }
    public SavedRevision create(T t) {
        Map<String,Object> props = MAPPER.convertValue(t, Map.class);
        // MyBean anotherBean = m.convertValue(props, MyBean.class);
        Document document = database.createDocument();
        try {
            return document.putProperties(props);
        } catch (CouchbaseLiteException e) {
            throw new NotAcceptableException(e);
        }

    }

    public void update(T t) {
        Document document = database.getDocument(t.getId());

        Map<String,Object> props = document.getProperties();
        props.putAll(MAPPER.convertValue(t, Map.class));
        try {
            document.putProperties(props);
        } catch (CouchbaseLiteException e) {
            throw new NotAcceptableException(e);
        }
    }

    public void remove(T t) {

        Document document = database.getDocument(t.getId());
        try {
            document.delete();
        } catch (CouchbaseLiteException e) {
            throw new NotAcceptableException(e);
        }
    }

    public T findById(String id) {

        Document document = database.getDocument(id);
        T o = (T)MAPPER.convertValue(document.getProperties(), instance.getClass());

        return o;
    }

    public List<T> find() {
        return null;
    }
}
