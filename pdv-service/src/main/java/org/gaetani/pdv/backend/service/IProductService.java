package org.gaetani.pdv.backend.service;

import org.gaetani.pdv.backend.model.Product;

/**
 * Created by gaetani on 7/31/16.
 */
public interface IProductService extends ICrudService<Product, String> {


}
