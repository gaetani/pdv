package org.gaetani.pdv.backend.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by gaetani on 7/31/16.
 */

@Data
public class Product implements Idable{

    @Getter
    private String _id;

    @Getter
    private String _rev;

    private String codigo;

    private String descricao;

    private String foto;

    private Boolean fracionado;

    private Date creation;

    private Date lastUpdate;


    public Product(){}


    @Override
    public String getId() {
        return _id;
    }

    @Override
    public void setId(String id) {
        _id = id;
    }
}
