package org.gaetani.pdv.backend.service;

import com.couchbase.lite.SavedRevision;

import java.util.List;

/**
 * Created by gaetani on 7/31/16.
 */
public interface ICrudService<T, String> {
    SavedRevision create(T t);
    void update(T t);
    void remove(T t);
    T findById(String pk);
    List<T> find();
}
