package org.gaetani.pdv.backend.model;

/**
 * Created by gaetani on 8/5/16.
 */
public interface Idable {
    String getId();
    void setId(String id);
}
