package org.gaetani.pdv.backend.service;

import com.couchbase.lite.SavedRevision;
import org.gaetani.pdv.backend.model.Product;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by gaetani on 8/5/16.
 */
public class ProductServiceTest {


    ProductService productService = new ProductService();

    @Test
    public void testCreate() {
        Product product = new Product();
        product.setDescricao("teste");
        SavedRevision revision = productService.create(product);

        Product product1 = productService.findById(revision.getDocument().getId());

        assertEquals(product.getDescricao(), product1.getDescricao());
    }


    @Test
    public void testUpdate() {

        Product product = new Product();
        product.setDescricao("teste");
        SavedRevision revision = productService.create(product);

        Product product1 = productService.findById(revision.getDocument().getId());



        assertEquals(product.getDescricao(), product1.getDescricao());
    }


    @Test
    public void testRemove() {

        Product product = new Product();
        product.setDescricao("teste");
        SavedRevision revision = productService.create(product);

        Product product1 = productService.findById(revision.getDocument().getId());

        productService.remove(product1);

        product1 = productService.findById(revision.getDocument().getId());
        assertNull(product1);
    }


    @Test
    public void testFindById() {
        Product product = new Product();
        product.setDescricao("teste");
        SavedRevision revision = productService.create(product);

        assertNotNull(revision.getDocument().getId());
    }
}